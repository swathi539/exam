// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

InternetButton b = InternetButton();
String level = "";
void setup() {
     b.begin();
     
   for(int i = 0;i< 3;i++)
   {
    b.allLedsOn(0,0,255);
    delay(400);
    b.allLedsOff();
    delay(400);
   }
   
   b.playSong("C4,8,E4,8,G4,8,C5,8,G5,4");
    delay(400);
    
    b.ledOn(1,0,0,255);
    b.ledOn(10,0,0,255);
    Particle.function("Memorize",memorize);

}

void loop() {
   
               
if(b.buttonOn(1)){
    
      if(level == "easy")
    {
        Particle.publish("sequence","RBYG",60,PUBLIC);
         b.allLedsOff();
         //R
        b.ledOn(1,255,0,0);
        delay(2000);
        // B
        b.ledOn(4,0,0,255);
        delay(2000);
        //Y
        b.ledOn(8,255,255,0);
        delay(2000);
        //W
        b.ledOn(11,0,128,0);
       
        delay(2000);
        b.allLedsOff();
                 
    }
    
     else if(level == "hard")
    {
    Particle.publish("sequence","GYBR",60,PUBLIC);

         b.allLedsOff();    

       //R
        b.ledOn(1,255,0,0);
        delay(300);
        // B
        b.ledOn(4,0,0,255);
        delay(300);
        //y
        b.ledOn(8,255,255,0);
        delay(300);
        //G
        b.ledOn(11,0,128,0);
        delay(300);
        b.allLedsOff();
                 
    
    }
    }
    
    
    if(b.buttonOn(2)){
        
          if(level == "easy")
    {
                Particle.publish("sequence","GYBR",60,PUBLIC);

        b.allLedsOff();
        // W
        b.ledOn(1,0,128,0);
        delay(2000);
        //Y
        b.ledOn(4,255,255,0);
        delay(2000);
        //B
        b.ledOn(8,0,255,255);
        delay(2000);
        //R
        b.ledOn(11,255,0,0);
        delay(2000);
        b.allLedsOff();
         
    }
    
    else if(level == "hard")
    {
        Particle.publish("sequence","RBYG",60,PUBLIC);
        b.allLedsOff();
     //G
        b.ledOn(1,0,128,0);
        delay(300);
     //Y
        b.ledOn(4,255,255,0);
        delay(300);
      //B
        b.ledOn(8,0,255,255);
        delay(300);
       //R 
        b.ledOn(11,255,0,0);
        delay(300);
        b.allLedsOff();
         
    }
    } 
    if(b.buttonOn(3)){

     if(level == "easy")
    {
        Particle.publish("sequence","BRYG",60,PUBLIC);

         b.allLedsOff(); 
          //B
        b.ledOn(1,0,0,255);
        delay(2000);
         //R
        b.ledOn(4,255,0,0);
        delay(2000);
        //R
        b.ledOn(8,255,255,0);
        delay(2000);
        //G
        b.ledOn(11,0,128,0);
        delay(2000);
        b.allLedsOff();

    }
    else if(level == "hard")
    {
     Particle.publish("sequence","GYRB",60,PUBLIC);

        b.allLedsOff();  
         //B
        b.ledOn(1,0,0,255);
        delay(300);
        //R
        b.ledOn(4,255,0,0);
        delay(300);
        //Y
        b.ledOn(8,255,255,0);
        delay(300);
        //G
        b.ledOn(11,0,128,0);
        delay(300);
        b.allLedsOff();
               
    }
    }
    if(b.buttonOn(4)){
     if(level == "easy")
    {
                Particle.publish("sequence","RBGY",60,PUBLIC);

         b.allLedsOff(); 
         //R
        b.ledOn(1,255,0,0);
        delay(2000);
        //B
        b.ledOn(4,0,0,255);
        delay(2000);
         //G
        b.ledOn(8,0,128,0);
        delay(2000);
        //R
        b.ledOn(11,255,255,0);
        delay(2000);
       
        b.allLedsOff();

    }
    else if(level == "hard")
    {
                Particle.publish("sequence","YGBR",60,PUBLIC);

        b.allLedsOff();  
        //R
        b.ledOn(1,255,0,0);
        delay(300);
        //B
        b.ledOn(4,0,0,255);
        delay(300);
        //G
        b.ledOn(8,0,128,0);
        delay(300);
        //Y
        b.ledOn(11,255,255,0);
        delay(300);
        b.allLedsOff();
               
    }
    }
  
}

  int memorize(String command)
    {
        level = command;
        Particle.publish("difficultylevel", level ,60,PUBLIC);
        return 1;
    }