package com.example.androidparticlestarter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class LevelActivity extends AppCompatActivity {


    SharedPreferences pref;
    SharedPreferences.Editor editor;
    TextView name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        name = findViewById(R.id.userName);


        // get the text from MainActivity
        Intent intent = getIntent();
        String text = intent.getStringExtra(Intent.EXTRA_TEXT);

       name.setText("Hello "+ text);


    }

    public void Easy(View view)
    {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("level", "easy");
        finish();
        startActivity(intent);
    }

    public void Hard(View view)
    {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("level", "hard");
        finish();
        startActivity(intent);
    }
}
