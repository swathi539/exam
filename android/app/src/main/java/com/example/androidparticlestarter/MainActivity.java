package com.example.androidparticlestarter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity {
    // MARK: Debug info
    private final String TAG="SWA";

    String sequence1;
    String sequence12;
    String clr1, clr2, clr3, clr4;
    String level;
    TextView result;
    Button restart;
    EditText color1, color2, color3, color4;

    // MARK: Particle Account Info
    private final String PARTICLE_USERNAME = "swathiramishetti13@gmail.com";
    private final String PARTICLE_PASSWORD = "Pass@123";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "270025001047363333343437";

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        color1 = findViewById(R.id.color1);
        color2 = findViewById(R.id.color2);
        color3 = findViewById(R.id.color3);
        color4 = findViewById(R.id.color4);
        result = findViewById(R.id.result);
        restart = findViewById(R.id.restart);
        level = getIntent().getStringExtra("level");
        ParticleCloudSDK.init(this.getApplicationContext());
        getDeviceFromCloud();
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {


                try {
                    subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents("sequence", new ParticleEventHandler() {

                        @Override
                        public void onEventError(Exception e) {
                        }
                        @Override
                        public void onEvent(String eventName, ParticleEvent particleEvent) {
                            sequence1 = particleEvent.dataPayload;

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return -1;
            }

            @Override
            public void onSuccess(Object o) {

                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }


    public void Result(View v)
    {
        clr1 = color1.getText().toString();
        clr2 = color2.getText().toString();
        clr3 = color3.getText().toString();
        clr4 = color4.getText().toString();
        sequence12 = clr1+clr2+clr3+clr4;

        if(sequence1 == null)
        {
            Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

                @Override
                public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {


                    try {
                        subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents("sequence", new ParticleEventHandler() {

                            @Override
                            public void onEventError(Exception e) {
                            }
                            @Override
                            public void onEvent(String eventName, ParticleEvent particleEvent) {
                                sequence1 = particleEvent.dataPayload;
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return -1;
                }

                @Override
                public void onSuccess(Object o) {

                    Log.d(TAG, "Successfully got device from Cloud");
                }

                @Override
                public void onFailure(ParticleCloudException exception) {
                    Log.d(TAG, exception.getBestMessage());
                }
            });

        }

        if (sequence1 != null && sequence12!= null && !sequence12.isEmpty())

            if (sequence1.equals(sequence12)) {
                result.setText("You Win");
                restart.setEnabled(true);

            } else {
                result.setText("You Loose");
                restart.setEnabled(true);

            }

    }
    public void restart(View v)
    {
        color1.setText("");
        color2.setText("");
        color3.setText("");
        color4.setText("");
        result.setText("");
        sequence1 = null;
        sequence12 = null;
        finishAffinity();
        startActivity(new Intent(getApplicationContext(), LevelActivity.class));
    }


    /**
     * Custom function to connect to the Particle Cloud and get the device
     */
    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                List<String> param = new ArrayList<String>();
                param.add(level);
                try {

                    mDevice.callFunction("Memorize",param);
                }
                catch(Exception e)
                {
                    Log.e("Memorizegame", e.toString());
                }

                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }






}
