package com.example.androidparticlestarter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class StartActivity extends AppCompatActivity {
    EditText Name;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String name = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Name = findViewById(R.id.Name);


    }



    // "Go to Second Activity" button click
    public void start(View view) {

        // get the text to pass
        Name = findViewById(R.id.Name);
        String textToPass = Name.getText().toString();

        // start the SecondActivity
        Intent intent = new Intent(this, LevelActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, textToPass);
        startActivity(intent);
    }
}
